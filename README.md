# ALP4 repository template

Use this template for the course ALP4.

## How To

* fork this repository
* add teaching assistant (TA) and your peers to this repository. 
* give TA reporter role and your peers maintainer (or developer)
* for every exercise, checkout either the *c-example* or the *java-example*
  branch and name the new branch accordingly

## Submission

In Whiteboard, add the corresponding commit hash in the comment field of the
exercise. 

Keep in mind this is specific to the course in Summer 2022! 

## Contributions

Feel free to open issues or submit merge requests for changes. Also you could
fork this to prepare templates for other courses.
